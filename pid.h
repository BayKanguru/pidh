/*
MIT License

Copyright (c) 2023 BayKanguru

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
*/

#ifndef PID_H_
#define PID_H_

typedef struct {
  float p, i, d, lp;
} PIDParameter_f;

typedef struct {
  int p, i, d, lp;
} PIDParameter_i;

float pidf(const float error, const float dt, const PIDParameter_f *K, float *I,
           float *prev);
int pidi(const int error, const int dt, const PIDParameter_i *K, int *I,
         int *prev, int S);

#endif // PID_H_

////////////////////////////////
// Implementation

#ifdef PID_IMPLEMENTATION

float pidf(const float error, const float dt, const PIDParameter_f *K, float *I,
           float *prev) {
  float D = (error - *prev) / dt;
  D = D < K->lp ? D : K->lp; // LPF
  *prev = error;

  return (K->p * error) + (K->i * (*I += error * dt)) + (K->d * D);
}

int pidi(const int error, const int dt, const PIDParameter_i *K, int *I,
         int *prev, int S) {
  int D = (error - *prev) * S * S / dt;
  D = S * D < K->lp ? D : K->lp; // LPF
  *prev = error;

  return ((K->p * error) + (K->i * (*I += error * dt / S)) +
         (K->d * D)) / S;
}

#endif // PID_IMPLEMENTATION
